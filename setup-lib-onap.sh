#!/bin/sh

__openstack() {
    __err=1
    __debug=
    __times=0
    while [ $__times -lt 16 -a ! $__err -eq 0 ]; do
        openstack $__debug "$@"
        __err=$?
        if [ $__err -eq 0 ]; then
            break
        fi
        __debug=" --debug "
        __times=`expr $__times + 1`
        if [ $__times -gt 1 ]; then
            echo "ERROR: openstack command failed: sleeping and trying again!"
            sleep 8
        fi
    done
}
