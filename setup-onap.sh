#!/bin/sh

set -x

# Grab our libs
DIRNAME=`dirname $0`
DIRNAME=`readlink -f $DIRNAME`
. "$DIRNAME/setup-lib.sh"
. "$DIRNAME/setup-lib-onap.sh"

if [ -f $OURDIR/onap-done ]; then
    exit 0
fi

logtstart "onap"

mkdir -p $OURDIR/onap
cd $OURDIR/onap

parallel-ssh -h $OURDIR/pssh.all-nodes -i \
    docker login -u docker -p docker nexus3.onap.org:10001

parallel-ssh -h $OURDIR/pssh.all-nodes -i \
    $SUDO ln -s /nfs /dockerdata-nfs

#
# Workaround a known bug in containerd 1.4.4-1, without getting in the way
# of kubespray defaults.
# https://github.com/kubernetes/kubernetes/issues/101056
# https://github.com/kubernetes/kubernetes/issues/45419
#
dpkg-query -W -f '${Version}' containerd.io | grep -q '1\.4\.4'
if [ $? -eq 0 ]; then
    parallel-ssh -h /local/setup/pssh.all-nodes -i \
        sudo apt-get install -y --allow-change-held-packages "containerd.io=1.4.6-1"

    parallel-ssh -h /local/setup/pssh.all-nodes -i \
        sudo systemctl restart docker
fi

#
# Wait for docker after possible restart above.
#
while true ; do
    $SUDO docker ps -a && break
done
while true ; do
    kubectl get namespace onap && break
    kubectl create namespace onap && break
done

#
# If we were given an OpenStack config, try to fetch some info from it.
#
OPENSTACKADMINOPENRC=$OURDIR/onap/admin-openrc.sh
if [ -n "$OPENSTACKPASSWORD" ]; then
    cat << EOF >$OPENSTACKADMINOPENRC
export OS_PROJECT_DOMAIN_NAME=$OPENSTACKDOMAIN
export OS_USER_DOMAIN_NAME=$OPENSTACKDOMAIN
export OS_PROJECT_NAME=$OPENSTACKTENANT
export OS_TENANT_NAME=$OPENSTACKTENANT
export OS_USERNAME=$OPENSTACKUSER
export OS_PASSWORD=$OPENSTACKPASSWORD
export OS_AUTH_URL=$OPENSTACKURL
export OS_IDENTITY_API_VERSION=3
export OS_IMAGE_API_VERSION=2
export OS_AUTH_TYPE=password
EOF
    . $OPENSTACKADMINOPENRC

    #
    # If $OPENSTACKIP, that is in our sharedvlan LAN, so create an endpoint
    # and service so that pods can access via short hostnames that the
    # OpenStack profile uses, and then across the shared vlan (via NAT).
    # (We have to hijack iptables control from the kubernetes plugin to ensure
    # it doesn't NAT us somewhere else.)
    #
    if [ -n "$OPENSTACKIP" ]; then
	oshostname=`echo "$OPENSTACKURL" | sed -nre 's|.*//([^:/]*).*$|\1|p'`
	grep -q "$OPENSTACKIP $oshostname" /etc/hosts.head
	if [ ! $? -eq 0 ]; then
	    echo "$OPENSTACKIP $oshostname" | $SUDO tee -a /etc/hosts.head
	    cat /etc/hosts.head | $SUDO tee -a /etc/hosts
	fi

	cat <<EOF >os-endpoint.yaml
apiVersion: v1
kind: Endpoints
metadata:
  name: $oshostname
subsets:
  - addresses:
    - ip: $OPENSTACKIP
    ports:
      - name: keystone
        port: 5000
      - name: nova
        port: 8774
EOF
	cat <<EOF >os-service.yaml
apiVersion: v1
kind: Service
metadata:
  name: $oshostname
spec:
  ports:
  - name: keystone
    protocol: TCP
    port: 5000
    targetPort: 5000
  - name: nova
    protocol: TCP
    port: 8774
    targetPort: 8774
EOF

	kubectl -n onap apply -f os-endpoint.yaml \
	    && kubectl -n onap apply -f os-service.yaml

	svip=`echo $SHAREDVLANADDRESS | cut -d/ -f1`
	snet=`echo $SHAREDVLANADDRESS | cut -d/ -f2`
	sbits=`netmask2prefix $snet`
	snip=`ipmask2networkip $svip $snet`

	cat <<EOF | $SUDO tee /etc/systemd/system/iptables-custom.service
[Unit]
Description=Custom iptables firewall
DefaultDependencies=yes
After=systemd-sysctl.service network.target

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/etc/iptables-custom/iptables.sh start
ExecStop=/etc/iptables-custom/iptables.sh stop

[Install]
WantedBy=multi-user.target
EOF
	$SUDO mkdir -p /etc/iptables-custom
	cat <<EOF | $SUDO tee /etc/iptables-custom/iptables.sh
#!/bin/sh

. /lib/lsb/init-functions

IPT=/sbin/iptables

start() {
    \$IPT -t nat -I POSTROUTING \
	-d $OPENSTACKIP/$sbits ! -s $svip/$sbits -j SNAT --to-source $svip
}

stop() {
    \$IPT -t nat -D POSTROUTING \
	-d $OPENSTACKIP/$sbits ! -s $svip/$sbits -j SNAT --to-source $svip
}
EOF
	cat <<'EOF' | $SUDO tee -a /etc/iptables-custom/iptables.sh

case "$1" in
    start)
        log_daemon_msg "Starting $DESC" "$NAME"
        start
        case "$?" in
            0) log_end_msg 0 ;;
            1) log_progress_msg "already started"
               log_end_msg 0 ;;
            *) log_end_msg 1 ;;
        esac
        ;;
    stop)
        log_daemon_msg "Stopping $DESC" "$NAME"
        stop
        case "$?" in
            0) log_end_msg 0 ;;
            1) log_progress_msg "already stopped"
               log_end_msg 0 ;;
            *) log_end_msg 1 ;;
        esac
        ;;
    restart|force-reload)
        $0 stop
        $0 start
        ;;
    *)
        echo "Usage: $SCRIPTNAME {start|stop|restart|panic|unpanic|stopfwd|startfwd}" >&2
        exit 3
        ;;
esac
EOF
	$SUDO chmod 755 /etc/iptables-custom/iptables.sh
	service_init_reload
	service_enable iptables-custom
	service_restart iptables-custom

	dataip=`getnodeip $HEAD $DATALAN`
	parallel-ssh -h /local/setup/pssh.all-nodes -i \
	    $SUDO ip route add $snip/$sbits via $dataip
    fi

    maybe_install_packages python3-openstackclient

    openstack network list
    if [ ! $? -eq 0 ]; then
	echo "ERROR: cannot invoke OpenStack API via $OPENSTACKURL; aborting!"
	exit 1
    fi

    #
    # Grab ONAP base images and add them to glance.
    #
    openstack image show xenial-server >/dev/null
    if [ ! $? -eq 0 ]; then
	get_url \
	    https://cloud-images.ubuntu.com/xenial/current/xenial-server-cloudimg-amd64-disk1.img \
	    xenial-server-disk1.img 64
	__openstack image create --disk-format qcow2 --public \
  	    --file xenial-server-disk1.img xenial-server
    fi
    openstack image show trusty-server >/dev/null
    if [ ! $? -eq 0 ]; then
	get_url \
	    https://cloud-images.ubuntu.com/trusty/current/trusty-server-cloudimg-amd64-disk1.img \
	    trusty-server-disk1.img 64
	__openstack image create --disk-format qcow2 --public \
	    --file trusty-server-disk1.img trusty-server
    fi

    #
    # Add the pubkey to openstack.
    #
    ONAP_KEY_NAME=onap
    ONAP_PUBKEY_FILE=$HOME/.ssh/onap_key.pub
    ONAP_PRIVKEY_FILE=$HOME/.ssh/onap_key
    openstack keypair show $ONAP_KEY_NAME
    if [ ! $? -eq 0 ]; then
	if [ -z "$ONAP_PUBKEY" ]; then
	    ssh-keygen -t rsa -b 2048 -N "" -f $ONAP_PRIVKEY_FILE
	    __openstack keypair create --public-key $ONAP_PUBKEY_FILE $ONAP_KEY_NAME
	else
	    echo "$ONAP_PUBKEY" > $ONAP_PUBKEY_FILE
	    __openstack keypair create --public-key $ONAP_PUBKEY_FILE $ONAP_KEY_NAME
	fi
    fi
    if [ -z "$ONAP_PUBKEY" ]; then
	ONAP_PUBKEY=`cat $ONAP_PUBKEY_FILE`
    fi
    if [ -z "$ONAP_PRIVKEY" ]; then
	ONAP_PRIVKEY=`cat $ONAP_PRIVKEY_FILE`
    fi
fi

#
# Set up a local chartmuseum to serve onap charts.
#
CHARTMUSEUM_STORAGEDIR=/storage/chartmuseum-storage
if [ ! -e ${CHARTMUSEUM_STORAGEDIR} ]; then
    $SUDO mkdir -p ${CHARTMUSEUM_STORAGEDIR}
    $SUDO chown $SWAPPER ${CHARTMUSEUM_STORAGEDIR}
    $SUDO chmod 775 ${CHARTMUSEUM_STORAGEDIR}
fi

while [ ! -e /usr/local/bin/chartmuseum ]; do
    # This becomes root on our behalf :-/
    # NB: we need >= 0.13 so that we can get the version that
    # can restrict bind to localhost.
    curl -o - https://raw.githubusercontent.com/helm/chartmuseum/main/scripts/get-chartmuseum \
	| bash
done
if [ ! -e /etc/systemd/system/chartmuseum-local.service ]; then
    cat <<EOF | $SUDO tee /etc/systemd/system/chartmuseum-local.service
[Unit]
Description=Chartmuseum on localhost
After=network.target network-online.target local-fs.target

[Service]
ExecStart=/usr/local/bin/chartmuseum --storage local --storage-local-rootdir ${CHARTMUSEUM_STORAGEDIR} --port 8879 --listen-host localhost
Restart=always
Type=simple

[Install]
WantedBy=multi-user.target
EOF
    service_init_reload
    service_enable chartmuseum-local
    service_restart chartmuseum-local
    sleep 2
    helm repo add local http://localhost:8879
fi

#
# Install cert-manager
#
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.yaml
curl -L -o kubectl-cert-manager.tar.gz \
    https://github.com/jetstack/cert-manager/releases/latest/download/kubectl-cert_manager-linux-amd64.tar.gz
tar -xvzf kubectl-cert-manager.tar.gz
$SUDO mv kubectl-cert_manager /usr/local/bin
kubectl cert-manager check api --wait=2m

#
# Install prometheus-stack
#
kubectl create namespace prometheus
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install prometheus prometheus-community/kube-prometheus-stack --namespace=prometheus
kubectl --namespace prometheus get pods -l "release=prometheus"

#
# Add the necessary helm-push chartmuseum plugin.
#
eval `helm env | grep HELM_PLUGINS`
if [ ! -e $HELM_PLUGINS/helm-push ]; then
    wget https://github.com/chartmuseum/helm-push/releases/download/v0.9.0/helm-push_0.9.0_linux_amd64.tar.gz
    mkdir -p $HELM_PLUGINS/helm-push
    tar -xzvf helm-push_0.9.0_linux_amd64.tar.gz \
        -C $HELM_PLUGINS/helm-push
fi

#
# Bootstrap via ONAP OOM
#
while [ ! -e $OURDIR/onap/oom ]; do
    git clone -b honolulu http://gerrit.onap.org/r/oom --recurse-submodules $OURDIR/onap/oom || sleep 10
done
helm search repo local | grep -q onap
if [ ! $? -eq 0 ]; then
    cd $OURDIR/onap/oom/kubernetes
    # Our controlling session is not a member of the `docker` group, so we
    # need a new session to build the charts, since they invoke docker...
    sudo su -c 'make SKIP_LINT=TRUE all' $SWAPPER
    if [ ! $? -eq 0 ]; then
	echo "ERROR: failed to build ONAP charts; aborting!"
	exit 1
    fi
    helm search repo local | grep -q onap
    if [ ! $? -eq 0 ]; then
	echo "ERROR: failed to upload ONAP charts; aborting!"
	exit 1
    fi

fi

#
# Copy ONAP helm deploy/undeploy plugins into place
#
if [ ! -e ~/.local/share/helm/plugins/deploy ]; then
    mkdir -p ~/.local/share/helm/plugins/
    cp -pR $OURDIR/onap/oom/kubernetes/helm/plugins/* ~/.local/share/helm/plugins/
fi

#
# Start building our overrides file for deploy.
#
OVERRIDES=$OURDIR/onap/onap-local-overrides.yaml

if [ ! -e $OVERRIDES ]; then
    #
    # NB: at honolulu, we need testing components enabled so that we can get the
    # ejbca cmpv2server for the sdnc.
    #
    cat <<EOF >$OVERRIDES
global:
  nodePortPrefix: 302
  nodePortPrefixExt: 304
  addTestingComponents: &testing true
  dataRootDir: /nfs
  persistence:
    mountPath: /nfs
    enableDefaultStorageclass: false
    parameters: {}
    storageclassProvisioner: kubernetes.io/no-provisioner
    volumeReclaimPolicy: Retain
  flavor: unlimited
#  debugEnabled: true

  pullPolicy: IfNotPresent

#  ingress:
#    enabled: false
#    virtualhost:
#      baseurl: $NFQDN

  centralizedLoggingEnabled: &centralizedLogging false

contrib:
  enabled: *testing
EOF

    if [ -n "$OPENSTACKPASSWORD" ]; then
	# Grab some openstack info
	O_TENANT_ID=`openstack project show $OPENSTACKTENANT | awk '/ id / { print \$4; }'`
	O_PUBLIC_NET_ID=`openstack network show ext-net | awk '/ id / { print \$4; }'`
	O_PRIVATE_NET_ID=`openstack network show flat-lan-1-net | awk '/ id / { print \$4; }'`
	O_PRIVATE_SUBNET_ID=`openstack subnet show flat-lan-1-subnet | awk '/ id / { print \$4; }'`
	O_PRIVATE_NET_CIDR=`openstack subnet show flat-lan-1-subnet | awk ' / cidr / { print \$4; }'`
	O_SECURITY_GROUP_ID=`openstack security group list | grep $O_TENANT_ID | grep default | awk ' / / { print \$2; }'`

	OPENSTACKBASEURL=$OPENSTACKURL
	OPENSTACKKEYSTONEAPIVERSION=`echo $OPENSTACKURL | sed -nre 's/^(.*)\/(v[^\/]*)$/\2/p'`
	if [ -n "$OPENSTACKKEYSTONEAPIVERSION" ]; then
	    OPENSTACKBASEURL=`echo $OPENSTACKURL | sed -nre 's/^(.*)\/(v[^\/]*)$/\1/p'`
	else
	    OPENSTACKKEYSTONEAPIVERSION=v3
	fi

	#
	# Get the right encrypted versions of the OpenStack plain-text password.
	# We need an openssl-encoded password, and java-encoded password.  Ugh.
	#
	O_SSLENCPASS=
	O_JAVAENCPASS=
	cd $OURDIR/onap/oom/kubernetes/so/resources/config/mso
	ONAPKEY=`cat encryption.key`
	O_SSLENCPASS=`echo -n "$OPENSTACKPASSWORD" | openssl aes-128-ecb -e -K "$ONAPKEY" -nosalt | xxd -c 256 -p`

	maybe_install_packages default-jdk

	cd $OURDIR/onap
	git clone http://gerrit.onap.org/r/integration
	cd integration/deployment/heat/onap-rke/scripts
	javac Crypto.java
	O_JAVAENCPASS=`java Crypto "$OPENSTACKPASSWORD" "$ONAPKEY"`
	cd $OURDIR/onap

	if [ -n "$O_JAVAENCPASS" ]; then
	    cat <<EOF >>$OVERRIDES

so:
  enabled: true
  replicaCount: 1
  liveness:
    enabled: true

  config:
    dmaapTopic: "AUTO"
    openStackUserName: "$OPENSTACKUSER"
    openStackRegion: "RegionOne"
    openStackKeyStoneUrl: "$OPENSTACKURL"
    openStackServiceTenantName: "service"
    openStackEncryptedPasswordHere: "$O_JAVAENCPASS"
    openStackKeystoneVersion: "KEYSTONE_V3"
    openStackProjectDomainName: "$OPENSTACKDOMAIN"
    openStackUserDomainName: "$OPENSTACKDOMAIN"
    openStackTenantId: "$O_TENANT_ID"

  so-catalog-db-adapter:
    config:
      openStackUserName: "$OPENSTACKUSER"
      openStackRegion: "RegionOne"
      openStackKeyStoneUrl: "$OPENSTACKURL"
      openStackServiceTenantName: "service"
      openStackEncryptedPasswordHere: "$O_JAVAENCPASS"
      openStackKeystoneVersion: "KEYSTONE_V3"
      openStackProjectDomainName: "$OPENSTACKDOMAIN"
      openStackUserDomainName: "$OPENSTACKDOMAIN"
      openStackTenantId: "$O_TENANT_ID"

appc:
  enabled: true
  config:
    openStackType: OpenStackProvider
    openStackName: OpenStack
    openStackKeyStoneUrl: "$OPENSTACKURL"
    openStackServiceTenantName: "service"
    openStackDomain: "$OPENSTACKDOMAIN"
    openStackUserName: "$OPENSTACKUSER"
    openStackEncryptedPassword: "$O_JAVAENCPASS"
    openStackKeystoneVersion: "KEYSTONE_V3"
    openStackProjectDomainName: "$OPENSTACKDOMAIN"
    openStackUserDomainName: "$OPENSTACKDOMAIN"
    openStackTenantId: "$O_TENANT_ID"
EOF
	fi

	if [ -n "$O_SSLENCPASS" ]; then
	    cat <<EOF >>$OVERRIDES

robot:
  enabled: true
  openStackKeyStoneUrl: "$OPENSTACKBASEURL"
  openStackKeystoneAPIVersion: "$OPENSTACKKEYSTONEAPIVERSION"
  openStackRegion: "RegionOne"
  openStackPublicNetworkName: "ext"
  openStackPublicNetId: "$O_PUBLIC_NET_ID"
  openStackProjectName: "$OPENSTACKTENANT"
  openStackTenantId: "$O_TENANT_ID"
  openStackUserName: "$OPENSTACKUSER"
  ubuntu16Image: "xenial-server"
  openStackPrivateNetId: "$O_PRIVATE_NET_ID"
  openStackPrivateSubnetId: "$O_PRIVATE_SUBNET_ID"
  openStackPrivateNetCidr: "$O_PRIVATE_NET_CIDR"
  openStackSecurityGroup: "$O_SECURITY_GROUP_ID"
  openStackOamNetworkCidrPrefix: "10.0"
  openStackKeystoneVersion: "KEYSTONE_V3"
  openStackProjectDomainName: "$OPENSTACKDOMAIN"
  openStackDomainId: "$OPENSTACKDOMAIN"
  openStackUserDomainName: "$OPENSTACKDOMAIN"
  openStackUserDomain: "$OPENSTACKDOMAIN"
  # vnfPubKey: ""
  # vnfPrivateKey: ""
  config:
    openStackEncryptedPasswordHere: "$O_SSLENCPASS"
EOF
	fi

	cat <<EOF >>$OVERRIDES

nbi:
  config:
    # openstack configuration
    openStackRegion: "RegionOne"
    openStackVNFTenantId: "$O_TENANT_ID"
EOF
    fi
fi

#
# Cons up a working minimal deployment.  Not minimal from a resource
# perspective, but from a component perspective.  These are derived from
# onap-minimal.yml, but it works on honolulu, unlike that config.
#
MINIMAL=$OURDIR/onap/onap-local-minimal.yaml
cat <<EOF >$MINIMAL
aai:
  enabled: true
aaf:
  # changed
  enabled: true
appc:
  # changed
  enabled: true
cassandra:
  enabled: true
  # cassandra as config'd requires >= 2 replicas
  #replicaCount: 1
clamp:
  enabled: false
cli:
  enabled: false
consul:
  enabled: false
contrib:
  enabled: false
cps:
  enabled: false
dcaegen2:
  enabled: false
dcaegen2-services:
  enabled: false
holmes:
  enabled: false
dmaap:
  enabled: true
esr:
  enabled: false
log:
  enabled: false
mariadb-galera:
  enabled: true
msb:
  enabled: false
multicloud:
  enabled: false
nbi:
  enabled: false
oof:
  enabled: false
platform:
  # changed
  enabled: true
policy:
  enabled: false
pomba:
  enabled: false
portal:
  enabled: true
robot:
  enabled: true
sdc:
  enabled: true
sdnc:
  enabled: true
sniro-emulator:
  enabled: false
so:
  enabled: true
uui:
  enabled: false
vid:
  enabled: true
vfc:
  enabled: false
vnfsdk:
  enabled: false
EOF

#
# Make sure mariadb-galera and cassandra are deployed first; many
# things depend on them; so avoid needless backoff/restarts.
#
helm deploy dev local/onap --namespace onap \
    --set global.masterPassword=root \
    -f $OURDIR/onap/oom/kubernetes/onap/resources/overrides/environment.yaml \
    -f $OVERRIDES \
    --set appc.enabled=false \
    --set so.enabled=false \
    --set mariadb-galera.enabled=true \
    --set cassandra.enabled=true \
    --set robot.enabled=false \
    --set contrib.enabled=false \
    --timeout 7200s

# Give kube API a chance to create all the objects
sleep 16
# Wait for our deployment to finish.
for i in 0 1 2 ; do
    kubectl -n onap wait --for=condition=ready pod --timeout=-1s \
	    -l statefulset.kubernetes.io/pod-name=dev-mariadb-galera-$i
done
kubectl -n onap rollout status statefulset/dev-mariadb-galera
for i in 0 1 2; do
    kubectl -n onap wait --for=condition=ready pod --timeout=-1s \
	    -l statefulset.kubernetes.io/pod-name=dev-cassandra-$i
done
kubectl -n onap rollout status statefulset/dev-cassandra

#
# Deploy ONAP in full.
#
# -f $MINIMAL
# -f $OURDIR/onap/oom/kubernetes/onap/resources/overrides/onap-vfw.yaml
# -f $OURDIR/onap/oom/kubernetes/onap/resources/overrides/onap-all.yaml
DEPEXTRA=
if [ "$ONAPDEPLOYMENTTYPE" = "vfw" ]; then
    DEPEXTRA="-f $OURDIR/onap/oom/kubernetes/onap/resources/overrides/onap-vfw.yaml"
elif [ "$ONAPDEPLOYMENTTYPE" = "full" ]; then
    DEPEXTRA="-f $OURDIR/onap/oom/kubernetes/onap/resources/overrides/onap-all.yaml"
fi
helm deploy dev local/onap --namespace onap \
    --set global.masterPassword=root \
    -f $OURDIR/onap/oom/kubernetes/onap/resources/overrides/environment.yaml \
    -f $OURDIR/onap/oom/kubernetes/onap/resources/overrides/oom-cert-service-environment.yaml \
    -f $MINIMAL \
    $DEPEXTRA \
    -f $OVERRIDES \
    --timeout 7200s

#
# (Try to) Wait for ONAP deployments to all become available.
#
lastpending=0
lastchange=`date +%s`
lastpending=0
echo "Waiting for ONAP deployments to complete..."
while true ; do
    deployments=`kubectl -n onap get deployments | tail +2`
    total=`echo -n "$deployments" | wc -l`
    avail=`echo -n "$deployments" | awk "/ / { print \\$4 }" | grep 1 | wc -l`
    pending=`echo -n "$deployments" | awk "/ / { print \\$4 }" | grep 0 | wc -l`

    if [ ! $pending -eq $lastpending ]; then
	lastchange=`date +%s`
	lastpending=$pending
	changed=1
    else
	changed=0
    fi
    now=`date +%s`
    changedelta=`expr $now - $lastchange`

    if [ $changed -eq 1 -o $changedelta -gt 600 ]; then
	date
	echo "deployments: $total"
	echo "  avail: $avail"
	echo "  pending: $pending"
    fi

    if [ $pending -eq 0 ]; then
	echo "ONAP deployments complete."
	break
    elif [ $changedelta -gt 1200 ]; then
	echo "No change to pending deployments ($pending) in past twenty minutes; aborting wait!"
	break
    fi

    sleep 60
done

$OURDIR/onap/oom/kubernetes/robot/ete-k8s.sh onap health 2>&1 \
    | tee $OURDIR/onap/health-checks-output.txt

PORTALIP=`kubectl -n onap get svc | sed -ne 's/^portal-app[ ]*LoadBalancer[ ]*[0-9\.]*[ ]*\([0-9\.]*\)[ ]*.*$/\1/p'`

cat <<EOF >$OURDIR/onap/etc-hosts
$MYIP     api.simpledemo.onap.org
$MYIP     portal.api.simpledemo.onap.org
$MYIP     portal-app.onap
$MYIP     vid.api.simpledemo.onap.org
$MYIP     sdc.api.fe.simpledemo.onap.org
$MYIP     sdc.api.simpledemo.onap.org
$MYIP     portal-sdk.simpledemo.onap.org
$MYIP     policy.api.simpledemo.onap.org
$MYIP     aai.api.sparky.simpledemo.onap.org
$MYIP     cli.api.simpledemo.onap.org
$MYIP     msb.api.discovery.simpledemo.onap.org
$MYIP     sdc.dcae.plugin.simpledemo.onap.org
$MYIP     sdc.workflow.plugin.simpledemo.onap.org
$MYIP     clamp.api.simpledemo.onap.org
EOF

cat <<EOF >$OURDIR/onap/onap.mail
Your ONAP instance has completed setup!  Output from OOM health checks is below.
Once you have modified your machine's /etc/hosts file with the contents below,
you can browse to https://portal.api.simpledemo.onap.org:30225/ONAPPORTAL/login.htm
and login with the 'demo' username and 'demo123456!' password.

If you selected the option to preload demo information, that process
is continuing.  Wait for the next email to tell you that has finished.

ONAP Kubernetes master IP: $MYIP
ONAP Portal floating IP: $PORTALIP

/etc/hosts file contents
========================

$MYIP     api.simpledemo.onap.org
$MYIP     portal.api.simpledemo.onap.org
$MYIP     portal-app.onap
$MYIP     vid.api.simpledemo.onap.org
$MYIP     sdc.api.fe.simpledemo.onap.org
$MYIP     sdc.api.simpledemo.onap.org
$MYIP     portal-sdk.simpledemo.onap.org
$MYIP     policy.api.simpledemo.onap.org
$MYIP     aai.api.sparky.simpledemo.onap.org
$MYIP     cli.api.simpledemo.onap.org
$MYIP     msb.api.discovery.simpledemo.onap.org
$MYIP     sdc.dcae.plugin.simpledemo.onap.org
$MYIP     sdc.workflow.plugin.simpledemo.onap.org
$MYIP     clamp.api.simpledemo.onap.org


ONAP Health Checks
==================

EOF
cat $OURDIR/onap/health-checks-output.txt >> $OURDIR/onap/onap.mail
cat $OURDIR/onap/onap.mail | mail -s "ONAP Instance Finished Setting Up" ${SWAPPER_EMAIL}

if [ $ONAPPRELOADDEMOS -eq 1 ]; then
    echo "test" | $OURDIR/onap/oom/kubernetes/robot/demo-k8s.sh onap init_robot
    $OURDIR/onap/oom/kubernetes/robot/demo-k8s.sh onap init_customer

    curl -k -X POST https://$MYIP:30200/vid/maintenance/category_parameter/owningEntity \
	 -H 'Accept: application/json' -H 'Content-Type: application/json' \
	 -H 'Postman-Token: 6a3181fa-c720-4c19-a952-79d68c9435cc' -H 'USER_ID: demo' \
	 -H 'X-FromAppId: robot-ete' -H 'X-TransactionId: robot-ete-bd65600d-8669-4903-8a14-af88203add38' \
	 -H 'cache-control: no-cache' -d '{ "options": ["OE-Demonstration"] }'
    curl -k -X POST https://$MYIP:30200/vid/maintenance/category_parameter/project \
	 -H 'Accept: application/json' -H 'Content-Type: application/json' \
	 -H 'Postman-Token: 6a3181fa-c720-4c19-a952-79d68c9435cc' -H 'USER_ID: demo' \
	 -H 'X-FromAppId: robot-ete' -H 'X-TransactionId: robot-ete-bd65600d-8669-4903-8a14-af88203add38' \
	 -H 'cache-control: no-cache' -d '{ "options": ["Project-Demonstration"] }'
    curl -k -X POST https://$MYIP:30200/vid/maintenance/category_parameter/platform \
	 -H 'Accept: application/json' -H 'Content-Type: application/json' \
	 -H 'Postman-Token: 6a3181fa-c720-4c19-a952-79d68c9435cc' -H 'USER_ID: demo' \
	 -H 'X-FromAppId: robot-ete' -H 'X-TransactionId: robot-ete-bd65600d-8669-4903-8a14-af88203add38' \
	 -H 'cache-control: no-cache' -d '{ "options": ["Platform-Demonstration"] }'
    curl -k -X POST https://$MYIP:30200/vid/maintenance/category_parameter/lineOfBusiness \
	 -H 'Accept: application/json' -H 'Content-Type: application/json' \
	 -H 'Postman-Token: 6a3181fa-c720-4c19-a952-79d68c9435cc' -H 'USER_ID: demo' \
	 -H 'X-FromAppId: robot-ete' -H 'X-TransactionId: robot-ete-bd65600d-8669-4903-8a14-af88203add38' \
	 -H 'cache-control: no-cache' -d '{ "options": ["LOB-Demonstration"] }'

    $OURDIR/onap/oom/kubernetes/robot/demo-k8s.sh onap distribute
    $OURDIR/onap/oom/kubernetes/robot/demo-k8s.sh onap distribute demoVFW
    # $OURDIR/onap/oom/kubernetes/robot/demo-k8s.sh onap instantiateVFW

    echo "The ONAP demo info has finished preloading.  You should be able to instantiate a vFW VNF element via \`$OURDIR/onap/oom/kubernetes/robot/demo-k8s.sh onap instantiateVFW\`." \
	| mail -s "ONAP Demos Finished Preloading" ${SWAPPER_EMAIL}
fi

logtend "onap"

touch $OURDIR/onap-done
